# This file contains example workflow for
# Functions defined in em_algorithm.R

# Set seed if required to have same results
# For random data
set.seed(123)

# Create two mixings of size 1000
# 1st sample illustrates a simple case
# 2nd sample is a more complex one
simple.case <- InitSampleData(
  mixing.probability = 0.25, 
  size = 1000, 
  x.mean.one = 1, 
  x.mean.two = 7, 
  y.mean.one = 3, 
  y.mean.two = 12)

complex.case <- InitSampleData(
  mixing.probability = 0.25, 
  size = 1000, 
  x.mean.one = 1, 
  x.mean.two = 4, 
  y.mean.one = 3, 
  y.mean.two = 6)

# Plot generated samples using ggplot2
simple.case.plot <- ggplot(simple.case, aes(x = x, y = y)) + geom_point()
simple.case.plot
complex.case.plot <- ggplot(complex.case, aes(x = x, y = y)) + geom_point()
complex.case.plot


# Run custom EM-algorithm
# And assess output
res.for.simple.case <- RunEM(
  simple.case$x, 
  mean.one.guess = 0, 
  mean.two.guess = 1, 
  mixing.probability = 0.5, 
  number.of.iterations = 10,
  verbose.output = TRUE)

res.for.complex.case <- RunEM(
  complex.case$x, 
  mean.one.guess = 0, 
  mean.two.guess = 1, 
  mixing.probability = 0.5, 
  number.of.iterations = 10,
  verbose.output = TRUE)

# Perform classification of mixing distributions
simple.case.classification <- PerformClassification(simple.case)
simple.case.classification

# Compare with original data
ggplot(simple.case, aes(x = x, y = y, color = toss)) + geom_point()

complex.case.classification <- PerformClassification(complex.case)
complex.case.classification

# Compare with original data
ggplot(complex.case, aes(x = x, y = y, color = toss)) + geom_point()

# Perform validation
# First install mixtools
install.packages("mixtools")
library("mixtools")

# Run normalmixEM from mixtools for simple case
# With same initial guesses
simple.case.mixtools <- normalmixEM(simple.case$x, mu = c(0,1), sigma=c(1,1), sd.constr=c(1,1))

# Check mixtools results
simple.case.mixtools$mu
simple.case.mixtools$lambda

# Run normalmixEM from mixtools for complex case
# With same initial guesses
complex.case.mixtools <- normalmixEM(complex.case$x, mu = c(0,1), sigma=c(1,1), sd.constr=c(1,1))

# Check mixtools results
complex.case.mixtools$mu
complex.case.mixtools$lambda
