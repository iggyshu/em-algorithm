library(testthat)

test_that("InitSampleData_CorrectArguments_ReturnsMixingOfSpecifiedSize",{
  # arrange
  size <- 1000
  
  # act
  df.test <- InitSampleData(0.25, size, 1, 7, 3, 5)
  
  # assert
  expect_equal(length(df.test$x), size)
  expect_equal(length(df.test$y), size)
})

test_that("RunEM_CorrectArguments_ProducesFineResults",{
  # arrange
  size <- 1000
  mean.one.x <- 1
  mean.two.x <- 7
  mean.one.y <- 3
  mean.two.y <- 5
  mixing.probability <- 0.25
  
  df.test <- InitSampleData(
    mixing.probability, 
    size, 
    mean.one.x, 
    mean.two.x, 
    mean.one.y, 
    mean.two.y)
  
  # act
  res.x <- RunEM(df.test$x)
  res.y <- RunEM(df.test$y)
  
  # assert
  expect_equal(round(res.x$mean.one), mean.one.x)
  expect_equal(round(res.x$mean.two), mean.two.x)
  expect_equal(round(res.y$mean.one), mean.one.y)
  expect_equal(round(res.y$mean.two), mean.two.y)
})

test_that("PerformClassification_CorrectArguments_ReturnsPlot",{
  # arrange
  size <- 1000
  mean.one.x <- 1
  mean.two.x <- 7
  mean.one.y <- 3
  mean.two.y <- 5
  mixing.probability <- 0.25
  
  df.test <- InitSampleData(
    mixing.probability, 
    size, 
    mean.one.x, 
    mean.two.x, 
    mean.one.y, 
    mean.two.y)
  
  # act
  res <- PerformClassification(df.test)
  
  # assert
  expect_equivalent(is.null(res), FALSE)
})